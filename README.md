
## ステージング環境構築手順

### 機能要件

| OS・ミドルウェア 	| ステージングバージョン 	| プロダクションバージョン 	|
|:----------------:	|:----------------------:	|:------------------------:	|
| CentOS           	| 5.11                   	| 5.8                      	|
| PHP              	| 5.1.6                  	| 5.1.6                    	|
| Mysql            	| 5.0.95                 	| 5.0.95                   	|
| apache           	| 2.2.3                  	| 2.2.3                    	|
| ImageMagick      	| 6.2.8                  	| 6.2.8                    	|


### 構築手順書


```sh
yum install gcc
yum install openssl-devel
```

#### image magick

```bash
# ref: http://mocmooz.blogspot.jp/2013/11/blog-post.html
cd /usr/local/src

wget https://launchpad.net/imagemagick/main/6.2.8-8/+download/ImageMagick-6.2.8-8.tar.gz
tar xfz ImageMagick-6.2.8-8.tar.gz
cd ImageMagick-6.2.8
./configure
make
make install
```


#### PHP + Apache + Mysql をインストール 

```
# ローカルに落としてインストールしているが、不要かも
cd /tmp

wget http://vault.centos.org/5.0/updates/i386/RPMS/php-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-cli-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-common-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-gd-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-mbstring-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-pgsql-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-xml-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-pdo-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-devel-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-mysql-5.1.6-7.el5.i386.rpm
yum localinstall php-5.1.6-7.el5.i386.rpm php-cli-5.1.6-7.el5.i386.rpm php-common-5.1.6-7.el5.i386.rpm php-gd-5.1.6-7.el5.i386.rpm php-mbstring-5.1.6-7.el5.i386.rpm php-pgsql-5.1.6-7.el5.i386.rpm php-xml-5.1.6-7.el5.i386.rpm php-pdo-5.1.6-7.el5.i386.rpm php-devel-5.1.6-7.el5.i386.rpm php-mysql-5.1.6-7.el5.i386.rpm

wget http://vault.centos.org/5.0/updates/i386/RPMS/php-bcmath-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-dba-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-imap-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-ldap-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-ncurses-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-odbc-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-snmp-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-soap-5.1.6-7.el5.i386.rpm
wget http://vault.centos.org/5.0/updates/i386/RPMS/php-xmlrpc-5.1.6-7.el5.i386.rpm
yum localinstall php-bcmath-5.1.6-7.el5.i386.rpm php-dba-5.1.6-7.el5.i386.rpm php-imap-5.1.6-7.el5.i386.rpm php-ldap-5.1.6-7.el5.i386.rpm php-ncurses-5.1.6-7.el5.i386.rpm php-odbc-5.1.6-7.el5.i386.rpm php-snmp-5.1.6-7.el5.i386.rpm php-soap-5.1.6-7.el5.i386.rpm php-xmlrpc-5.1.6-7.el5.i386.rpm

# モジュールインストール
yum install php-pear
yum install php-readline
yum install php-soap
yum install php-mcrypt
yum install php-mhash
yum install php-pecl-Fileinfo
```


#### mysql のインストール・設定


```
# php-mysql の依存で mysql はインストール済み
yum install mysql-server

chkconfig mysqld on

# パスワードの設定はソースを見てから
# https://www.server-world.info/query?os=CentOS_5&p=mysql
```


#### apache のインストール・設定

※ php の依存関係で既に apache はインストール済み

```
# モジュールのインストール
yum install mod_ssl

chkconfig httpd on 
```

```
vi /etc/httpd/conf.d/vhosts.conf
# 下記の内容を追加する
```


```
NameVirtualHost *:80

<Directory /var/www/vhosts/*/>
        AllowOverride FileInfo AuthConfig Limit Options
        Options MultiViews SymLinksIfOwnerMatch Includes ExecCGI
        <Limit GET POST OPTIONS PROPFIND>
                Order allow,deny
                Allow from all
        </Limit>
        <LimitExcept GET POST OPTIONS PROPFIND>
                Order deny,allow
                Deny from all
        </LimitExcept>
</Directory>

<VirtualHost *:80>
    ServerName dev.example.com
    ServerAdmin root@localhost
    DocumentRoot /var/www/vhosts/example.com/WWW
    ErrorLog /var/www/vhosts/example.com/LOG/error_log
    CustomLog /var/www/vhosts/example.com/LOG/access_log combined
    ScriptAlias /cgi-bin/ "/var/www/vhosts/example.com/cgi-bin/"
</VirtualHost>
```

#### SSL通信を利用するため、オレオレ証明書を発行

```
# 保存場所は雑
# ref: http://d.hatena.ne.jp/ozuma/20130511/1368284304

cd /etc/httpd/conf.d/
openssl genrsa 2048 > server.key
openssl req -new -key server.key > server.csr
openssl x509 -days 3650 -req -signkey server.key < server.csr > server.crt
mkdir ssl.crt
mkdir ssl.key -m 700
mv ./server.crt ssl.crt
mv ./server.key ssl.key
chmod 400 ssl.key/server.key

vi /etc/httpd/conf.d/ssl.conf
# 下記の内容を追加
```

```
#
# This is the Apache server configuration file providing SSL support.
# It contains the configuration directives to instruct the server how to
# serve pages over an https connection. For detailing information about these 
# directives see <URL:http://httpd.apache.org/docs/2.2/mod/mod_ssl.html>
# 
# Do NOT simply read the instructions in here without understanding
# what they do.  They're here only as hints or reminders.  If you are unsure
# consult the online docs. You have been warned.  
#

LoadModule ssl_module modules/mod_ssl.so

#
# When we also provide SSL we have to listen to the 
# the HTTPS port in addition.
#
Listen 443

##
##  SSL Global Context
##
##  All SSL configuration in this context applies both to
##  the main server and all SSL-enabled virtual hosts.
##

#
#   Some MIME-types for downloading Certificates and CRLs
#
AddType application/x-x509-ca-cert .crt
AddType application/x-pkcs7-crl    .crl

#   Pass Phrase Dialog:
#   Configure the pass phrase gathering process.
#   The filtering dialog program (`builtin' is a internal
#   terminal dialog) has to provide the pass phrase on stdout.
SSLPassPhraseDialog  builtin

#   Inter-Process Session Cache:
#   Configure the SSL Session Cache: First the mechanism 
#   to use and second the expiring timeout (in seconds).
#SSLSessionCache        dc:UNIX:/var/cache/mod_ssl/distcache
SSLSessionCache         shmcb:/var/cache/mod_ssl/scache(512000)
SSLSessionCacheTimeout  300

#   Semaphore:
#   Configure the path to the mutual exclusion semaphore the
#   SSL engine uses internally for inter-process synchronization. 
SSLMutex default

#   Pseudo Random Number Generator (PRNG):
#   Configure one or more sources to seed the PRNG of the 
#   SSL library. The seed data should be of good random quality.
#   WARNING! On some platforms /dev/random blocks if not enough entropy
#   is available. This means you then cannot use the /dev/random device
#   because it would lead to very long connection times (as long as
#   it requires to make more entropy available). But usually those
#   platforms additionally provide a /dev/urandom device which doesn't
#   block. So, if available, use this one instead. Read the mod_ssl User
#   Manual for more details.
SSLRandomSeed startup file:/dev/urandom  256
SSLRandomSeed connect builtin
#SSLRandomSeed startup file:/dev/random  512
#SSLRandomSeed connect file:/dev/random  512
#SSLRandomSeed connect file:/dev/urandom 512

#
# Use "SSLCryptoDevice" to enable any supported hardware
# accelerators. Use "openssl engine -v" to list supported
# engine names.  NOTE: If you enable an accelerator and the
# server does not start, consult the error logs and ensure
# your accelerator is functioning properly. 
#
SSLCryptoDevice builtin
#SSLCryptoDevice ubsec

##
## SSL Virtual Host Context
##

<VirtualHost _default_:443>
ServerName stg.example.com
ServerAdmin root@localhost
DocumentRoot /var/www/vhosts/example.com/WWW
ErrorLog /var/www/vhosts/example.com/LOG/error_log
CustomLog /var/www/vhosts/example.com/LOG/access_log combined
ScriptAlias /cgi-bin/ "/var/www/vhosts/example.com/cgi-bin/"

SSLEngine on
SSLProtocol all -SSLv2
SSLCertificateFile /etc/httpd/conf.d/ssl.crt/server.crt
SSLCertificateKeyFile /etc/httpd/conf.d/ssl.key/server.key
</VirtualHost>
```

動かすために一部ソースコードを編集し、動作を確認


### ソースを配置

```
yum install epel-release
yum install git
```

#### ソースの取得

```
# @TODO 2017-07-11
# git から落とせなかったので、取り急ぎscpで
# 後日、石井さんにデプロイキーを有効化して頂く
# mkdir git && cd git
# git clone git@bitbucket.org:example/example_com.git
# mkdir /var/www/vhosts/
# cp ~/git/example_com /var/www/vhosts/example.com
```

##### scp（仮対応）

```
# サーバのドキュメントルートを作成
mkdir /var/www/vhosts/
```

```
# ローカルで最新のソースを取得し、stgにコピーする
# cd /tmp/
# git clone git@bitbucket.org:example/example_com.git
# scp -r /tmp/example_com stg.example.com:/tmp
scp <Local Directory> stg.example.com:/tmp
```

#### 本番データを取得（画像を利用するため）

** 現状、gitに画像などを保存していないため、下記の対応が必要になる。
また、この対応は一時的であり、根本解決が必要。（2017-07-11） **


```
# 1. 本番環境のソースコードをローカルに取得する
scp root@0.0.0.0:/var/www/vhosts/example.com /tmp/

# 2. ローカルからステージングに送る
scp /tmp/example.com stg.example.com:/tmp/
```

```
# in stg.example_com

# 3. 本番データを `/var/www/images` に配置（`images`は便宜上）
ll /tmp/example.com
cp -R /tmp/example.com /var/www/vhosts/images

cd /var/www/vhosts/example.com/WWW/common
ln -s /var/www/vhosts/example.com/WWW/common/img img

cd /var/www/vhosts/example.com/app/var/
# gitにignoreしきれなかったファイルが存在しているため、imageディレクトリを削除
rm -rf images
ln -s /var/www/vhosts/images/app/var/images images   

cd /var/www/vhosts/example.com/WWW
# gitにignoreしきれなかったファイルが存在しているため、imageディレクトリを削除
rm -rf images
ln -s /var/www/vhosts/images/WWW/images images


# permission などを整理
chown -R apache:apache /var/www/vhosts
chmod -R 777 /var/www/vhosts
```

### データソースを更新（DB）

```
# mysql -u root
> set password for root@localhost=password('poM39U4Q'); 
> create database example_com;
> exit
# mysql -u root -p example_com < mysqldump20170608.sql 
# @TODO後に個人情報などを書き換える
```

### その他

* git外管理の画像の同期（※ 定期的に以下のスクリプトを実行するのが良いのか => 根本解決が必要）

```
# 古いimgデータに差分があれば変更する
rsync -avzn \
--include='*/' \
--include='*.jpg' \
--include='*.jpeg' \
--include='*.png' \
--include='*.gif'\
--exclude='*' \
/tmp/example.com /Users/k_matsushima/Developments/example_com/images
```

